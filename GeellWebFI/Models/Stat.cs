﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GeelWebF.Models
{
    public class Stat
    {
        public int Id { get; set; }
        public string Name { get; set; } = "Geel";
        public int Height { get; set; }
        public int Wall { get; set; }
        public int RedWall { get; set; }
        public int DeadCount { get; set; }

    }
    public class StatResponse
    {
        public List<Stat> Stats;
    }
    public class Position
    { 
        public string name { get; set; }
        public float x { get; set; }
        public float y { get; set; }
    }
    public class Indexer
    {
        public int index { get; set; }
    }
    
}