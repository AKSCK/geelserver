﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GeelWebF.Models
{
    public enum ItemType
    {
        Skin,
        Bonus
    }

    public class Item
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public string Photo { get; set; }
        public string ItemId { get; set; }
        public ItemType Type { get; set; }
        public bool Donat { get; set; }
    
    }

    class ItemContext : DbContext
    {
        public ItemContext()
            : base("DbConnection")
        { }

        public DbSet<Item> Items { get; set; }

    }
}