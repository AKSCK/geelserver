﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GeelWebF.Models
{
    
    class StatContext : DbContext
    {
        public StatContext()
            : base("DbConnection")
        { }

        public DbSet<Stat> Stats { get; set; }
    }
}