﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GeelWebF.Models;

namespace GeellWebFI.Controllers
{
    public class ShopController : Controller
    {
        // GET: Shop
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult GetItemList()
        {
            using (var db = new ItemContext())
            {
                return Json(db.Items.ToList());
            }
                
        } 
    }
}