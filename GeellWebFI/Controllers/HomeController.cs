﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Numerics;
using GeelWebF.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using GeellWebFI.Models;

namespace GeellWebFI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        static public List<Stat> Stats = new List<Stat>();
        static string KeyCript = "J4Nj9)qzf#((v85G";
        static public List<Position> Positions = new List<Position>();
        public static Position position1 = new Position();
        public static Position position2 = new Position();
        public static StringBuilder builder = new StringBuilder();
        
        public string Debug()
        {
            return builder.ToString();
        }

        [HttpPost]
        public JsonResult SendMyPosition(Position p)
        {
            // builder.Append(p.name + p.y + "<br>");
            for (int i = 0; i < Positions.Count; i++)
            {
                if (Positions[i].name == p.name)
                {
                    Positions[i] = p;
                    return Json("{\"succes\":true}" + p.name, JsonRequestBehavior.DenyGet);
                }
            }
            Positions.Add(p);

            return Json("{\"succes\":true}" + p.name, JsonRequestBehavior.DenyGet);
        }

        public JsonResult ClearPositions()
        {
            Positions = new List<Position>();
            Stats = new List<Stat>();
            builder = new StringBuilder();
            return Json("{\"succes\":true}", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetMyPosition(Position p)
        {
            for (int i = 0; i < Positions.Count; i++)
            {
                if (Positions[i].name == p.name)
                {
                    return Json(Positions[i], JsonRequestBehavior.DenyGet);
                }
            }
            return Json(new Position(), JsonRequestBehavior.DenyGet);
        }
        [HttpPost]
        public JsonResult GetAllPosition()
        {
            return Json(Positions, JsonRequestBehavior.DenyGet);
        }
        [HttpPost]
        public JsonResult GetNewPosition(Indexer indexer)
        {
            int index = indexer.index;
            // builder.Append(indexer.ToString() + "<br>");
            List<Position> pos = new List<Position>();
            for (int i = index; i < Positions.Count; i++)
            {

                pos.Add(Positions[i]);

            }
            return Json(pos, JsonRequestBehavior.DenyGet);
        }
        [HttpPost]
        public JsonResult SetStatistics(Stat stat)
        {
            builder.Append("SetStatistics");
            builder.Append(stat.Name + " " + stat.Height + "<br>");

            if (Request.Headers.Get("DefHash") == GenerateHash(stat))
            {
                using (StatContext db = new StatContext())
                {
                    db.Stats.Add(stat);
                    db.SaveChanges();
                    return Json("{\"succes\":true}" + Request.Headers.Get("DefHash"), JsonRequestBehavior.DenyGet);
                }

            }


            return Json("{\"succes\":false}" + Request.Headers.Get("DefHash"), JsonRequestBehavior.DenyGet);
        }
        public static byte[] ComputeHmacsha1(byte[] data, byte[] key)
        {
            using (var hmac = new HMACSHA1(key))
            {
                return hmac.ComputeHash(data);
            }
        }
        public static string GenerateHash(Stat stat)
        {
            byte[] byteArrayName = Encoding.UTF8.GetBytes(stat.Name + stat.Height + "" + stat.DeadCount);
            byte[] byteArray2 = Encoding.UTF8.GetBytes(KeyCript);
            var bytehash = ComputeHmacsha1(byteArrayName, byteArray2);
            return new BigInteger(bytehash).ToString();
        }
        [HttpPost]
        public JsonResult UpdateLastStatistics(Stat stat)
        {
            if (Request.Headers.Get("DefHash") == GenerateHash(stat))
            {
                using (StatContext db = new StatContext())
                {
                    builder.Append("Start UpdateLastStatistics  " + stat.Name + "<br>");
                    builder.Append("Start UpdateLastStatistics to count  " + (db.Stats.Where(st => st.Name == stat.Name).Count().ToString()) + "<br>");

                    if (db.Stats.Where(st => st.Name == stat.Name).Count() == 0)
                    {
                        builder.Append("New UpdateLastStatistics" + stat.Name + "<br>");
                        db.Stats.Add(stat);
                        db.SaveChanges();

                        return Json("{\"succes\":true}" + stat.Name, JsonRequestBehavior.DenyGet);
                    }
                    Stat s = new Stat();
                    builder.Append("UpdateLastStatistics" + "<br>");
                    try
                    {

                        s = db.Stats.Where(st => st.Name == stat.Name).ToList().Last();
                    }
                    catch (Exception e)
                    {
                        builder.Append("UpdateLastStatistics" + e.ToString() + "<br>");
                    }
                    if (s.Height < stat.Height)
                    {
                        s.DeadCount = stat.DeadCount;
                        s.Height = stat.Height;
                        s.RedWall = stat.RedWall;
                        s.Wall = stat.Wall;

                        db.SaveChanges();
                    }

                }
            }
            return Json("{\"succes\":false}" + stat.Name, JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetStatistics()
        {
            List<Stat> st;
            using (StatContext db = new StatContext())
            {
                //  st = db.Stats.ToList();
                st = db.Stats.OrderBy(s => s.Height).ToList();
            }
            return Json(st, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult CheckPosition(Position s)
        {
            if (s.name == "Geel1")
            {
                position1 = s;
                return Json(position2, JsonRequestBehavior.DenyGet);
            }
            else
            {
                position2 = s;
                return Json(position1, JsonRequestBehavior.DenyGet);
            }

        }
    }
}
