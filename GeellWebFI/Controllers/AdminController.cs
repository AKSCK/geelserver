﻿using GeellWebFI.Models;
using GeelWebF.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GeellWebFI.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        [HttpGet, Authorize(Roles = "admin")]
        public ActionResult Index()
        {
            ItemContext itemContext = new ItemContext();
            
            return View(itemContext.Items);
        }
        //public string AdminRole(ApplicationDbContext context)
        //{

        //    var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));

        //    var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

        //    // создаем две роли
        //    var role1 = new IdentityRole { Name = "admin" };
        //    var role2 = new IdentityRole { Name = "user" };

        //    // добавляем роли в бд
        //    roleManager.Create(role1);
        //    roleManager.Create(role2);

        //    // создаем пользователей
        //    var admin = new ApplicationUser { Email = "rekym@yandex.ru", UserName = "rekym@yandex.ru" };
        //    string password = "3k4-NkL-sYW-Ypg";
        //    var result = userManager.Create(admin, password);

        //    // если создание пользователя прошло успешно
        //    if (result.Succeeded)
        //    {
        //        // добавляем для пользователя роль
        //        userManager.AddToRole(admin.Id, role1.Name);
        //        userManager.AddToRole(admin.Id, role2.Name);
        //    }

        //    return "";
        //}
        [HttpGet, Authorize(Roles = "admin")]
        public ActionResult Create()
        {
            var it = new Item();
            return View(it);
        }
        [HttpPost, Authorize(Roles = "admin")]
        public RedirectResult Create(Item it)
        {
            ItemContext itemContext = new ItemContext();
            itemContext.Items.Add(it);
            itemContext.SaveChanges();
            return  Redirect("/Admin/Index");
        }
        [HttpPost, Authorize(Roles = "admin")]
        public ActionResult Upload()
        {
            string fileName = null;
            foreach (string file in Request.Files)
            {
                var upload = Request.Files[file];
                if (upload != null)
                {
                    // получаем имя файла
                    fileName = System.IO.Path.GetFileName(upload.FileName);
                    // сохраняем файл в папку Files в проекте
                    upload.SaveAs(Server.MapPath("~/Files/Items/" + fileName));
                }
            }
            return Json("/Files/Items/" + fileName);
        }
        [HttpGet,Authorize(Roles = "admin")]
        public ActionResult Edit(Item it)
        {
            ItemContext itemContext = new ItemContext();
            it = itemContext.Items.Find(it.Id);
            return View(it);
        }
        [HttpPost, ActionName("Edit"), Authorize(Roles = "admin")]
        public ActionResult EditConfirmed(Item it)
        {
            ItemContext itemContext = new ItemContext();
            itemContext.Entry(it).State = System.Data.Entity.EntityState.Modified;
            itemContext.SaveChanges();
            return Redirect("/Admin/Index");
        }
        [HttpGet, Authorize(Roles = "admin"), Authorize(Roles = "admin")]
        public ActionResult Delete(Item it)
        {
            ItemContext itemContext = new ItemContext();
            it = itemContext.Items.Find(it.Id);
            return View(it);
        }
        [HttpPost, ActionName("Delete"), Authorize(Roles = "admin")]
        public ActionResult DeleteConfirmed(Item it)
        {
            ItemContext itemContext = new ItemContext();
            it = itemContext.Items.Find(it.Id);
            if(it != null)
            {
                itemContext.Items.Remove(it);
                itemContext.SaveChanges();
            }
            
            return Redirect("/Admin/Index");
        }
    }
}