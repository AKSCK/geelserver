namespace GeellWebFI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPriceMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Items",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Name = c.String(),
                    Price = c.Double(),
                    Photo = c.String(),
                    ItemId = c.String(),
                    Type = c.Int(nullable: false),
                    Donat = c.Boolean(nullable: false),
                })
                .PrimaryKey(t => t.Id);

        }


        public override void Down()
        {
            DropTable("dbo.Items");
        }
    }
}
