﻿using Microsoft.Extensions.Configuration;
using UDP.Server;

namespace Geel.Position.App
{
    public class AppSettings : IServerSetting
    {
        public int SubscribePort { get; private set; }
        public int ReceivePort { get; private set; }

        private readonly IConfiguration _configuration;

        public AppSettings(IConfiguration configuration)
        {
            _configuration = configuration;
            SubscribePort = int.Parse(configuration[nameof(SubscribePort)]);
            ReceivePort = int.Parse(configuration[nameof(ReceivePort)]);
        }

    }
}
