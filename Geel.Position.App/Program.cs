﻿using Geel.Position.App.Services;
using Geel.Position.App.Services.lmpl;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace Geel.Position.App
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            var host = CreateHostBuilder(args).Build();

            var runner = host.Services.GetService<IPositionUdpService>();
            Task.WaitAll(runner.Run());
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
           Host.CreateDefaultBuilder(args)
               .ConfigureWebHostDefaults(webBuilder =>
               {
                   webBuilder.UseStartup<Startup>();
               });
    }
}
