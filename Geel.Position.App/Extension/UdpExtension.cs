﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Geel.Position.App.Extension
{
    static class UdpExtension
    {
        public static async Task<(T, IPEndPoint)> ReceiveAsync<T>(this UdpClient udpClient)
        {
            var receiveResult = await udpClient.ReceiveAsync();
            var receiveByte = receiveResult.Buffer;
            var json = Encoding.UTF8.GetString(receiveByte);
            T ob = JsonConvert.DeserializeObject<T>(json);
            return (ob, receiveResult.RemoteEndPoint);
        }
    }
}
