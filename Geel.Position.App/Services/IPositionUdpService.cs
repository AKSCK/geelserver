﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Geel.Position.App.Services
{
    public interface IPositionUdpService
    {
        Task Run();
        Task Run(CancellationToken token);
    }
}
