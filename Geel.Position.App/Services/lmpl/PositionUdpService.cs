﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Geel.Position.App.Domain;
using Geel.Position.App.Extension;
using Newtonsoft.Json;
using UDP.Domain;
using UDP.Server;

namespace Geel.Position.App.Services.lmpl
{
    public class PositionUdpService :Server, IPositionUdpService 
    {
        IServerSetting _setting;
        IAppLogger _logger;

        public PositionUdpService(IServerSetting setting, IAppLogger logger):base(setting,logger)
        {
            _setting = setting;
            _logger = logger;
        }
        public async Task Run()
        {
            await Run(CancellationToken.None);
        }

        protected override async Task ProcessReceiveData(byte[] data, IPEndPoint end, UdpClient server)
        {
            var a = Encoding.UTF8.GetString(data);
            await base.ProcessReceiveData(data, end, server);
        }
    }
}
