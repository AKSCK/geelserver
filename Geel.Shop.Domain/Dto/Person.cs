﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Geel.Shop.Domain.Dto
{
    public class Person
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
    }
}
