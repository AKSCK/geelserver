﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Geel.Shop.Domain.Dto
{
    public class ResponseJwt
    {
       public string AccessToken { get; set; }
       public string Username { get; set; }
    }
}
