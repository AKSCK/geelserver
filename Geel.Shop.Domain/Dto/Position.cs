﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Geel.Shop.Domain.Dto
{
    public class Position
    {
        public DateTime date { get; set; }
        public string name { get; set; }
        public float x { get; set; }
        public float y { get; set; }
    }
}
