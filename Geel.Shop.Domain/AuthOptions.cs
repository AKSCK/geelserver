﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Text;

namespace Geel.Shop.Domain
{
    public class AuthOptions
    {
        public const string ISSUER = "CeelServer"; // издатель токена
        public const string AUDIENCE = "GeelClient"; // потребитель токена
        const string KEY = "Geel_secretkey!19231";   // ключ для шифрации
        public const int LIFETIME = 129600; // время жизни токена - 3 месяца
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
