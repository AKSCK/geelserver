﻿using Geel.Shop.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Geel.Shop.Domain.Response
{
    public class StatResponse
    {
        public List<Stat> Stats;
    }
}
