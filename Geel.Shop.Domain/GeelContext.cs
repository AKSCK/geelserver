﻿using Geel.Shop.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Geel.Shop.Domain
{
    public class GeelContext: DbContext
    {
        public DbSet<Item> Items { get; set; }
        public DbSet<Stat> Stats { get; set; }
        public GeelContext()
        {
            Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=geel;Username=postgres;Password=example");
        }

    }
}
