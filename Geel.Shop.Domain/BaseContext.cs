﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

namespace Geel.Shop.Domain
{
    public class BaseContext<T>
    {
        readonly protected string _path;
        SemaphoreSlim semaphore = new SemaphoreSlim(1);
        public BaseContext(string path)
        {
            _path = path;
            if (File.Exists(_path))
            {
                var itemsJson = File.ReadAllText(_path);
                Items = JsonConvert.DeserializeObject<List<T>>(itemsJson);
            }
            if (Items == null)
            {
                Items = new List<T>();
            }
        }

        public void SaveChanges()
        {
            semaphore.Wait();
            File.WriteAllText(_path, JsonConvert.SerializeObject(Items));
            semaphore.Release();
        }

        public List<T> Items { get; set; }
    }
}
