﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Geel.Shop.Domain.Entities
{
    public enum ItemType
    {
        Skin,
        Bonus
    }

    public class Item
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public string Photo { get; set; }
        public string ItemId { get; set; }
        public ItemType Type { get; set; }
        public bool Donat { get; set; }
    
    }

    
}