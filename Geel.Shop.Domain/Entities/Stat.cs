﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Geel.Shop.Domain.Entities
{
    public class Stat
    {
        public int Id { get; set; }
        public string Name { get; set; } = "Geel";
        public int Height { get; set; }
        public int Wall { get; set; }
        public int RedWall { get; set; }
        public int DeadCount { get; set; }

    }
    
}