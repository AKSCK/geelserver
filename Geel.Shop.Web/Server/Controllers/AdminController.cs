﻿using Geel.Shop.Domain;
using Geel.Shop.Domain.Dto;
using Geel.Shop.Domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;


namespace Geel.Shop.Web.Server.Controllers
{
    [Route("[controller]/[action]")]
    public class AdminController : Controller
    {

        IWebHostEnvironment _appEnvironment;
        GeelContext _itemContext;
        public AdminController(IWebHostEnvironment appEnvironment, GeelContext itemContext)
        {
            _appEnvironment = appEnvironment;
            _itemContext = itemContext;
        }

        // GET: Admin
        //[HttpGet, Authorize(Roles = "admin")]
        [HttpGet, Authorize(Roles ="admin")]
        public IEnumerable<Item> Index()
        {
            return _itemContext.Items;
        }
        [HttpPost, Authorize(Roles = "admin")]
        public ActionResult Create([FromBody]Item it)
        {
             _itemContext.Items.Add(it);
            _itemContext.SaveChanges();
            return Ok(it);
        }

        [HttpPost, Authorize(Roles = "admin")]
        public async Task<IActionResult> AddFile(IFormFile uploadedFile)
        {
            if (uploadedFile != null)
            {
                if (!Directory.Exists("Files/Items"))
                {
                    Directory.CreateDirectory("Files/Items");
                }
                // сохраняем файл в папку Files в каталоге wwwroot
                using (var fileStream = new FileStream("Files/Items/" + uploadedFile.FileName, FileMode.Create))
                {
                    await uploadedFile.CopyToAsync(fileStream);
                }
            }
            return RedirectToAction();
        }
        [HttpPost, Route("{name}"), Authorize(Roles = "admin")]
        public async Task<string> TestFile(string name)
        {
            
            var directory = @"/Files/Items/";
            if (!Directory.Exists(_appEnvironment.WebRootPath + directory))
            {
                Directory.CreateDirectory(_appEnvironment.WebRootPath + directory);
            }
            var uploadedFile = Request?.Body;
            var path = directory + name + DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss") + ".png";
            // сохраняем файл в папку Files в каталоге wwwroot
            using (var fileStream = new FileStream(_appEnvironment.WebRootPath + path, FileMode.Create))
            {
                await uploadedFile.CopyToAsync(fileStream);
            }
            return path;
        }
        [HttpPost, Authorize(Roles = "admin")]
        public ActionResult Edit([FromBody] Item it)
        {
            var itOld = _itemContext.Items.FirstOrDefault(i => i.Id == it.Id );
            if(itOld != null)
            {
                itOld.ItemId = it.ItemId ?? itOld?.ItemId;
                itOld.Name = it.Name ?? itOld?.Name;
                itOld.Photo = it.Photo ?? itOld?.Photo;
                itOld.Price = it.Price;
                itOld.Type = it.Type;
                itOld.Donat = it.Donat;
                _itemContext.SaveChanges();
                return Ok(it);
            }
            return NotFound();
        }

        [HttpPost, Authorize(Roles = "admin")]
        public ActionResult Delete([FromBody] Item it)
        {
            var deleteItem = _itemContext.Items.FirstOrDefault(i => i.Id == it.Id);
            if(deleteItem == null)
            {
                return NotFound();
            }
            _itemContext.Items.Remove(deleteItem);
            _itemContext.SaveChanges();
            return Ok(it);
        }
    }
}