﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Numerics;
using Microsoft.AspNetCore.Mvc;
using Geel.Shop.Domain.Dto;
using Geel.Shop.Domain;
using Geel.Shop.Domain.Entities;
using Geel.Shop.Domain.Request;

namespace GeellWebFI.Controllers
{
    [Route("[controller]/[action]")]
    public class HomeController : Controller
    {
        static GeelContext _statContext;
        static string KeyCript = "J4Nj9)qzf#((v85G";
        static public List<Position> Positions = new List<Position>();
        static public Dictionary<string, Item> Items = new Dictionary<string, Item>();
        public static Position position1 = new Position();
        public static Position position2 = new Position();
        public static StringBuilder builder = new StringBuilder();

        public HomeController(GeelContext statContext)
        {
            _statContext = statContext;
        }
        public string Debug()
        {
            return builder.ToString();
        }

        [HttpPost]
        public JsonResult SendMyPosition([FromBody] Position p)
        {
            // builder.Append(p.name + p.y + "<br>");
            for (int i = 0; i < Positions.Count; i++)
            {
                if (Positions[i].name == p.name)
                {
                    p.date = DateTime.UtcNow;
                    Positions[i] = p;
                    return Json("{\"succes\":true}" + p.name);
                }
            }
            Positions.Add(p);

            return Json("{\"succes\":true}" + p.name);
        }

        public JsonResult ClearPositions()
        {
            Positions = new List<Position>();
            builder = new StringBuilder();
            return Json("{\"succes\":true}");
        }
        [HttpPost]
        public JsonResult GetMyPosition([FromBody] Position p)
        {
            for (int i = 0; i < Positions.Count; i++)
            {
                if (Positions[i].name == p.name)
                {
                    return Json(Positions[i]);
                }
            }
            return Json(new Position());
        }
        [HttpPost]
        public JsonResult GetAllPosition()
        {
            return Json(Positions);
        }
        public JsonResult SendItem([FromBody] Item item,[FromQuery]string name)
        {
            Items[name] = item;
            return Json("{\"succes\":true}");
        }

        public JsonResult GetItem([FromQuery] string name)
        {
            if (!Items.ContainsKey(name))
            {
                return Json(null);
            }
            return Json(Items[name]);
        }

        [HttpPost]
        public JsonResult GetNewPosition([FromBody] Indexer indexer)
        {
            int index = indexer.index;
            // builder.Append(indexer.ToString() + "<br>");
            List<Position> pos = new List<Position>();
            for (int i = index; i < Positions.Count; i++)
            {
                pos.Add(Positions[i]);
            }
            return Json(pos);
        }
        [HttpPost]
        public JsonResult SetStatistics([FromBody] Stat stat)
        {
            builder.Append("SetStatistics");
            builder.Append(stat.Name + " " + stat.Height + "<br>");

            if (Request.Headers["DefHash"] == GenerateHash(stat))
            {
                
                {
                    _statContext.Stats.Add(stat);
                    _statContext.SaveChanges();
                    return Json("{\"succes\":true}" + Request.Headers["DefHash"]);
                }

            }


            return Json("{\"succes\":false}" + Request.Headers["DefHash"]);
        }
        public static byte[] ComputeHmacsha1(byte[] data, byte[] key)
        {
            using (var hmac = new HMACSHA1(key))
            {
                return hmac.ComputeHash(data);
            }
        }
        public static string GenerateHash(Stat stat)
        {
            byte[] byteArrayName = Encoding.UTF8.GetBytes(stat.Name + stat.Height + "" + stat.DeadCount);
            byte[] byteArray2 = Encoding.UTF8.GetBytes(KeyCript);
            var bytehash = ComputeHmacsha1(byteArrayName, byteArray2);
            return new BigInteger(bytehash).ToString();
        }
        [HttpPost]
        public JsonResult UpdateLastStatistics([FromBody] Stat stat)
        {
            if (Request.Headers["DefHash"] == GenerateHash(stat))
            {
                
                {
                    builder.Append("Start UpdateLastStatistics  " + stat.Name + "<br>");
                    builder.Append("Start UpdateLastStatistics to count  " + (_statContext.Stats.Where(st => st.Name == stat.Name).Count().ToString()) + "<br>");

                    if (_statContext.Stats.Where(st => st.Name == stat.Name).Count() == 0)
                    {
                        builder.Append("New UpdateLastStatistics" + stat.Name + "<br>");
                        _statContext.Stats.Add(stat);
                        _statContext.SaveChanges();

                        return Json("{\"succes\":true}" + stat.Name);
                    }
                    Stat s = new Stat();
                    builder.Append("UpdateLastStatistics" + "<br>");
                    try
                    {
                        s = _statContext.Stats.Where(st => st.Name == stat.Name).ToList().Last();
                    }
                    catch (Exception e)
                    {
                        builder.Append("UpdateLastStatistics" + e.ToString() + "<br>");
                    }
                    if (s.Height < stat.Height)
                    {
                        s.DeadCount = stat.DeadCount;
                        s.Height = stat.Height;
                        s.RedWall = stat.RedWall;
                        s.Wall = stat.Wall;

                        _statContext.SaveChanges();
                        return Json("{\"succes\":true}" + stat.Name);
                    }

                }

            }
            return Json("{\"succes\":false}" + stat.Name);
        }

        public JsonResult GetStatistics()
        {
            List<Stat> st;
            
            {
                st = _statContext.Stats.OrderBy(s => s.Height).ToList();
            }
            return Json(st);
        }
        [HttpPost]
        public JsonResult CheckPosition([FromBody] Position s)
        {
            if (s.name == "Geel1")
            {
                position1 = s;
                return Json(position2);
            }
            else
            {
                position2 = s;
                return Json(position1);
            }

        }
    }
}
