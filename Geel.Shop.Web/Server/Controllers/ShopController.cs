﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Geel.Shop.Domain;
using Geel.Shop.Domain.Dto;
using Geel.Shop.Domain.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Geel.Shop.Web.Server.Controllers
{
    [Route("[controller]/[action]")]
    [ApiController]
    public class ShopController : Controller
    {
        GeelContext _itemContext;
        public ShopController(GeelContext itemContext)
        {
            _itemContext = itemContext;
        }
        [HttpPost]
        public List<Item> GetItemList()
        {
            return _itemContext.Items.ToList();

        }
    }
}
